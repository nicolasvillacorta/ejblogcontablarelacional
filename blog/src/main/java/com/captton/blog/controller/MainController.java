package com.captton.blog.controller;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.captton.blog.model.InputComment;
import com.captton.blog.model.InputPostPost;
import com.captton.blog.model.Post;
import com.captton.blog.model.PostComment;
import com.captton.blog.model.Users;
import com.captton.blog.repository.DaoPost;
import com.captton.blog.repository.DaoPostComment;
import com.captton.blog.repository.DaoUsers;

@RestController
@RequestMapping({"/social"})
public class MainController {

	@Autowired
	private DaoUsers daousers;
	@Autowired
	private DaoPost daopost;
	@Autowired
	private DaoPostComment daopostcomment;
		
	@PostMapping(path = {"/users"})
	public Users create(@RequestBody Users user) {
		
		return daousers.save(user);
		
	}
	
	@PostMapping(path = {"/post"})
	public ResponseEntity<Object> create(@RequestBody InputPostPost post) {
		
		Users us1 = daousers.findById(post.getIdUser()).orElse(null);
		Post pos = new Post();
		
		if(us1!=null) {
			
			pos.setUser(us1);
			pos.setComment(post.getPost().getComment());
			pos.setTitle(post.getPost().getTitle());
			
			Post posteo = daopost.save(pos);
			
			
			JSONObject obj = new JSONObject();
			obj.put("error", 0);
			obj.put("message", posteo.getId());
			
			return ResponseEntity.ok().body(obj.toString());
		}else {
			
			JSONObject obj = new JSONObject();
			obj.put("error", 1);
			obj.put("message", "User not found");
			
			return ResponseEntity.ok().body(obj.toString());
		}
	}
	
	@PostMapping(path = {"/comment"})
	public ResponseEntity<Object> createComment(@RequestBody InputComment com) {
		
		Users us1 = daousers.findById(com.getIdUser()).orElse(null);
		Post pos = daopost.findById(com.getIdPost()).orElse(null);
		
		if(us1!=null && pos!=null) {
			
			PostComment postcomment = new PostComment();
			
			postcomment.setUser(us1);
			postcomment.setPost(pos);
			postcomment.setComment(com.getComment());
			
			PostComment commentResult = daopostcomment.save(postcomment);
			
			
			JSONObject obj = new JSONObject();
			obj.put("error", 0);
			obj.put("message", commentResult.getId());
			
			return ResponseEntity.ok().body(obj.toString());
		}else {
			
			JSONObject obj = new JSONObject();
			obj.put("error", 1);
			obj.put("message", "User/Post not found");
			
			return ResponseEntity.ok().body(obj.toString());
		}
	}
	
	@GetMapping(path = {"/users"})
	public ResponseEntity<Object> getUsers(){
		
		List<Users> usersFound = daousers.findAll();
		JSONArray json_array =  new JSONArray();
		
		if(!usersFound.isEmpty()) {
			
			for(Users us: usersFound) {
				
				JSONObject aux = new JSONObject();
				aux.put("name", us.getName());
				aux.put("fechanacimiento", us.getFechanac());
				json_array.put(aux);
				
				
			}
		}
		
		JSONObject obj = new JSONObject();
		obj.put("error", "0");
		obj.put("results", json_array);
		
		return ResponseEntity.ok().body(obj.toString());
		
	}
	
	@GetMapping(path = {"/comment/{idpost}"})
	public ResponseEntity<Object> getCommentsFromPost(@PathVariable Long idpost){
		
		Post post = daopost.findById(idpost).orElse(null);
		List<PostComment> commentsFound = daopostcomment.findByPost(post);
		
		JSONArray json_array =  new JSONArray();
		
		if(!commentsFound.isEmpty()) {
			
			for(PostComment com: commentsFound) {
				
				JSONObject aux = new JSONObject();
				aux.put("comment", com.getComment());
				aux.put("usuario", com.getUser().getName());
				json_array.put(aux);
			}
		}
		
		JSONObject obj = new JSONObject();
		obj.put("error", "0");
		obj.put("results", json_array);
		
		return ResponseEntity.ok().body(obj.toString());
		
	}
	
	
	
	
	
	
	
	
	
}
