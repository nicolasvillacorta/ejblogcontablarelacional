package com.captton.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.blog.model.Users;

public interface DaoUsers extends JpaRepository<Users, Long> {

}
