package com.captton.blog.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.blog.model.Post;
import com.captton.blog.model.PostComment;

public interface DaoPostComment extends JpaRepository<PostComment, Long> {

	public List<PostComment> findByPost(Post post);
}
