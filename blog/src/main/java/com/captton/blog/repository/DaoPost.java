package com.captton.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.blog.model.Post;

public interface DaoPost extends JpaRepository<Post, Long> {

}
